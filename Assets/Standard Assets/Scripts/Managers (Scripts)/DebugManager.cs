﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace ControlShift
{
	public class DebugManager : SingletonMonoBehaviour<DebugManager>
	{
		public Canvas canvas;
		public Text debugTextPrefab;
		public Transform debugTextsParent;
		List<string> entries = new List<string>();
		
		public override void Start ()
		{
			base.Start ();
			canvas.enabled = false;
		}
		
		public void AddEntry (object o, float duration = Mathf.Infinity)
		{
			StartCoroutine(ShowEntry (o, duration));
		}
		
		IEnumerator ShowEntry (object o, float duration)
		{
			string text = o.ToString();
			Text debugText = Instantiate(debugTextPrefab, debugTextsParent);
			debugText.text = text;
			Debug.Log(text);
			canvas.enabled = true;
			entries.Add(text);
			yield return new WaitForSecondsRealtime(duration);
			int entryIndex = entries.IndexOf(text);
			RemoveEntry (entryIndex);
		}
		
		public bool RemoveEntry (int index)
		{
			bool output = debugTextsParent.childCount > index;
			if (output)
			{
				if (debugTextsParent.childCount == 1)
					canvas.enabled = false;
				Destroy(debugTextsParent.GetChild(index).gameObject);
				entries.RemoveAt(index);
			}
			return output;
		}
	}
}