using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using WindowsInput;
using WindowsInput.Native;
using System;
using UnityEngine.UI;
using Extensions;
using System.IO;
using Point = System.Drawing.Point;
using Object = UnityEngine.Object;
using UnityEngine.InputSystem;
using TMPro;

namespace ControlShift
{
	public class GameManager : SingletonMonoBehaviour<GameManager>
	{
		public const char REPLACE_WITH_ZERO_WIDTH_CHAR = '_';
		public const char ZERO_WIDTH_CHAR = '​';
		public static WindowsInputDeviceStateAdaptor inputDeviceStateAdaptor;
		public static WindowsInput.InputSimulator inputSimulator;
		public static List<VirtualKeyCode> pressedKeys = new List<VirtualKeyCode>();
		public RectTransform cursorTrs;
		public static Array allKeys;
		public static IUpdatable[] updatables = new IUpdatable[0];
		public static Dictionary<Type, object> singletons = new Dictionary<Type, object>();
		public int birthYear;
		public int birthMonth;
		public int expectedLifespan;
		// public TMP_Text yearsText;
		// public TMP_Text monthsText;
		// public TMP_Text daysText;
		// public TMP_Text hoursText;
		// public TMP_Text minutesText;
		// public TMP_Text secondsText;
		public TMP_Text[] texts = new TMP_Text[0];
		public int[] decimalPlaces = new int[0];
		public VirtualKeyCode[] toggleKeys = new VirtualKeyCode[0];
		bool[] toggleKeysPressed = new bool[0];
		bool[] toggleKeysPreviouslyPressed = new bool[0];
		Func<double>[] functions = new Func<double>[0];
		float defaultWindowAlpha;
		DateTime birthdate;
		DateTime deathDate;
		TimeSpan lifeSpan;

		public override void Start ()
		{
			base.Start ();
			inputDeviceStateAdaptor = new WindowsInputDeviceStateAdaptor();
			inputSimulator = new WindowsInput.InputSimulator();
			allKeys = Enum.GetValues(typeof(VirtualKeyCode));
			defaultWindowAlpha = GetSingleton<TransparentWindow>().alpha;
			birthdate = new DateTime(birthYear, birthMonth, 1, 1, 1, 1, DateTimeKind.Utc);
			deathDate = birthdate.AddYears(expectedLifespan);
			lifeSpan = deathDate - birthdate;
			for (int i = 0; i < texts.Length; i ++)
			{
				TMP_Text text = texts[i];
				text.text = text.text.Replace(REPLACE_WITH_ZERO_WIDTH_CHAR, ZERO_WIDTH_CHAR);
			}
			functions = new Func<double>[7];
			functions[0] = GetYears;
			functions[1] = GetMonths;
			functions[2] = GetDays;
			functions[3] = GetHours;
			functions[4] = GetMinutes;
			functions[5] = GetSeconds;
			functions[6] = GetPercent;
			toggleKeysPressed = new bool[toggleKeys.Length];
			toggleKeysPreviouslyPressed = new bool[toggleKeys.Length];
		}
		
		void Update ()
		{
			InputSystem.Update ();
			for (int i = 0; i < allKeys.Length; i ++)
			{
				VirtualKeyCode key = (VirtualKeyCode) allKeys.GetValue(i);
				if (!GameManager.pressedKeys.Contains(key) && GameManager.inputDeviceStateAdaptor.IsKeyDown(key))
					GameManager.pressedKeys.Add(key);
				else if (GameManager.pressedKeys.Contains(key) && !GameManager.inputDeviceStateAdaptor.IsKeyDown(key))
					GameManager.pressedKeys.Remove(key);
			}
			HandleToggleOverlay ();
			foreach (IUpdatable updatable in updatables)
				updatable.DoUpdate ();
			for (int i = 0; i < texts.Length; i ++)
			{
				TMP_Text text = texts[i];
				bool toggleKeyPressed = pressedKeys.Contains(VirtualKeyCode.CONTROL) && pressedKeys.Contains(VirtualKeyCode.SHIFT) && pressedKeys.Contains(toggleKeys[i]);
				if (toggleKeyPressed && !toggleKeysPreviouslyPressed[i])
					text.enabled = !text.enabled;
				int indexOfZeroWidhChar = text.text.IndexOf(ZERO_WIDTH_CHAR);
				text.text = text.text.RemoveBetween("" + ZERO_WIDTH_CHAR, "" + ZERO_WIDTH_CHAR);
				text.text = text.text.Insert(indexOfZeroWidhChar, "" + ZERO_WIDTH_CHAR + ZERO_WIDTH_CHAR);
				text.text = text.text.Insert(indexOfZeroWidhChar + 1, functions[i].Invoke().ToString("F" + decimalPlaces[i]));
				toggleKeysPreviouslyPressed[i] = toggleKeyPressed;
			}
		}

		double GetYears ()
		{
			return -1;
		}

		double GetMonths ()
		{
			return -1;
		}

		double GetDays ()
		{
			return (deathDate - DateTime.UtcNow).TotalDays;
		}

		double GetHours ()
		{
			return (deathDate - DateTime.UtcNow).TotalHours;
		}

		double GetMinutes ()
		{
			return (deathDate - DateTime.UtcNow).TotalMinutes;
		}

		double GetSeconds ()
		{
			return (deathDate - DateTime.UtcNow).TotalSeconds;
		}

		double GetPercent ()
		{
			return (lifeSpan.TotalSeconds - (deathDate - DateTime.UtcNow).TotalSeconds) / lifeSpan.TotalSeconds * 100;
		}

		public static T Clone<T> (T obj, Transform parent = null) where T : Object
		{
			return (T) Instantiate(obj, parent);
		}

		public static void _DestroyImmediate (Object obj)
		{
			DestroyImmediate(obj);
		}

		bool toggleOverlayInput;
		bool previousToggleOverlayInput;
		void HandleToggleOverlay ()
		{
			toggleOverlayInput = InputManager.ToggleOverlayInput;
			if (toggleOverlayInput && !previousToggleOverlayInput)
			{
				if (GetSingleton<TransparentWindow>().alpha == 0)
					GetSingleton<TransparentWindow>().alpha = defaultWindowAlpha;
				else
					GetSingleton<TransparentWindow>().alpha = 0;
			}
			previousToggleOverlayInput = toggleOverlayInput;
		}
	
		public virtual void OnApplicationQuit ()
		{
			foreach (VirtualKeyCode key in pressedKeys)
				inputSimulator.Keyboard.KeyUp(key);
		}

		public static T GetSingleton<T> ()
		{
			if (!singletons.ContainsKey(typeof(T)))
				return GetSingleton<T>(FindObjectsOfType<Object>());
			else
			{
				if (singletons[typeof(T)] == null || singletons[typeof(T)].Equals(default(T)))
				{
					T singleton = GetSingleton<T>(FindObjectsOfType<Object>());
					singletons[typeof(T)] = singleton;
					return singleton;
				}
				else
					return (T) singletons[typeof(T)];
			}
		}

		public static T GetSingleton<T> (Object[] objects)
		{
			if (typeof(T).IsSubclassOf(typeof(Object)))
			{
				foreach (Object obj in objects)
				{
					if (obj is T)
					{
						singletons.Remove(typeof(T));
						singletons.Add(typeof(T), obj);
						break;
					}
				}
			}
			if (singletons.ContainsKey(typeof(T)))
				return (T) singletons[typeof(T)];
			else
				return default(T);
		}
		
		// [Serializable]
		// public class CursorEntry
		// {
		// 	public Sprite spriteFill;
		// 	public Sprite spriteOutline;
		// 	public CursorType type;
		// 	public static CursorType activeType;
			
		// 	public virtual void SetAsActiveCursor ()
		// 	{
		// 		GameManager.GetSingleton<GameManager>().cursorImageFill.sprite = spriteFill;
		// 		GameManager.GetSingleton<GameManager>().cursorImageOutline.sprite = spriteOutline;
		// 		if (spriteFill != null)
		// 			GameManager.GetSingleton<GameManager>().cursorRectTrs.pivot = spriteFill.pivot.Divide(new Vector2(spriteFill.texture.width, spriteFill.texture.height));
		// 		GameManager.GetSingleton<GameManager>().previousCursorType = activeType;
		// 		activeType = type;
		// 	}
			
		// 	public virtual bool IsActive ()
		// 	{
		// 		return activeType == type;
		// 	}
		// }
		
		// public enum CursorType
		// {
		// 	Default,
		// 	AddTeleportPoint,
		// 	ChooseColor
		// }
		
		// [Serializable]
		// public class GameModifier
		// {
		// 	public string name;
		// 	public bool isActive;
		// }
	}
}