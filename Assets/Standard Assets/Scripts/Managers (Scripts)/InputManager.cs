using UnityEngine;
using UnityEngine.InputSystem;
using System;
using WindowsInput.Native;

namespace ControlShift
{
	public class InputManager : MonoBehaviour
	{
		public static bool ToggleOverlayInput
		{
			get
			{
				return GameManager.pressedKeys.Contains(VirtualKeyCode.CONTROL) && GameManager.pressedKeys.Contains(VirtualKeyCode.SHIFT) && GameManager.pressedKeys.Contains(VirtualKeyCode.VK_D);
			}
		}
		
		[Serializable]
		public class InputButtonGroup
		{
			public InputButton[] buttons;
			public bool AllButtonsActivated
			{
				get
				{
					bool output = buttons.Length > 0;
					foreach (InputButton button in buttons)
						output &= button.IsActivated;
					return output;
				}
			}
			public bool AnyButtonActivated
			{
				get
				{
					bool output = false;
					foreach (InputButton button in buttons)
						output |= button.IsActivated;
					return output;
				}
			}
			// [HideInInspector]
			public bool previousAllButtonsActivated;
			// [HideInInspector]
			public bool previousAnyButtonsActivated;
			// [HideInInspector]
			public bool isPressing;
			public bool cancellable;
			// [HideInInspector]
			public bool cancelled;
		}
		
		[Serializable]
		public class InputButton
		{
			public KeyCode key;
			public State actionToActivate;
			public bool IsActivated
			{
				get
				{
					bool output = false;
					switch (actionToActivate)
					{
						case State.Pressing:
							output = Input.GetKeyDown(key);
							break;
						case State.Holding:
							output = Input.GetKey(key);
							break;
						case State.NotHolding:
							output = !Input.GetKey(key);
							break;
						case State.Releasing:
							output = Input.GetKeyUp(key);
							break;
					}
					return output;
				}
			}
	
			public enum State
			{
				Pressing,
				Holding,
				NotHolding,
				Releasing
			}
		}
	}
}