﻿using System;
using System.Runtime.InteropServices;

namespace ControlShift
{
	public class MouseOperations
	{
		[Flags]
		public enum MouseEventFlags
		{
			LeftDown = 0x00000002,
			LeftUp = 0x00000004,
			MiddleDown = 0x00000020,
			MiddleUp = 0x00000040,
			Move = 0x00000001,
			Absolute = 0x00008000,
			RightDown = 0x00000008,
			RightUp = 0x00000010
		}
		
		[DllImport("user32.dll", EntryPoint = "SetCursorPos")]
		[return: MarshalAs(UnmanagedType.Bool)]
		static extern bool SetCursorPos (int x, int y);      
		
		[DllImport("user32.dll")]
		[return: MarshalAs(UnmanagedType.Bool)]
		static extern bool GetCursorPos (out MousePoint lpMousePoint);
		
		[DllImport("user32.dll")]
		static extern void mouse_event (int dwFlags, int dx, int dy, int dwData, int dwExtraInfo);
		
		public static void SetCursorPosition (int x, int y) 
		{
			SetCursorPos (x, y);
		}
		
		public static void SetCursorPosition (MousePoint point)
		{
			if (point.xRelativeToCurrentPos)
				point.x += GetCursorPosition().x;
			if (point.yRelativeToCurrentPos)
				point.y += GetCursorPosition().y;
			SetCursorPos (point.x, point.y);
		}
		
		public static MousePoint GetCursorPosition ()
		{
			MousePoint currentMousePoint;
			if (!GetCursorPos(out currentMousePoint))
				currentMousePoint = new MousePoint(0, 0);
			return currentMousePoint;
		}
		
		public static void MouseEvent (MouseEventFlags value)
		{
			MousePoint position = GetCursorPosition();
			mouse_event((int) value, position.x, position.y, 0, 0);
		}
		
		[System.Serializable]
		[StructLayout(LayoutKind.Sequential)]
		public struct MousePoint
		{
			public int x;
			public int y;
			public bool xRelativeToCurrentPos;
			public bool yRelativeToCurrentPos;
			
			public MousePoint (int x, int y)
			{
				this.x = x;
				this.y = y;
				this.xRelativeToCurrentPos = false;
				this.yRelativeToCurrentPos = false;
			}
			
			public MousePoint (int x, int y, bool xRelativeToCurrentPos, bool yRelativeToCurrentPos)
			{
				this.x = x;
				this.y = y;
				this.xRelativeToCurrentPos = xRelativeToCurrentPos;
				this.yRelativeToCurrentPos = yRelativeToCurrentPos;
			}
	
			public MousePoint (int x, int y, bool isRelativePoint)
			{
				this.x = x;
				this.y = y;
				this.xRelativeToCurrentPos = isRelativePoint;
				this.yRelativeToCurrentPos = isRelativePoint;
			}
	
			public bool Equals (MousePoint mousePoint)
			{
				return x == mousePoint.x && y == mousePoint.y && xRelativeToCurrentPos == mousePoint.xRelativeToCurrentPos && yRelativeToCurrentPos == mousePoint.yRelativeToCurrentPos;
			}
			
			public override string ToString ()
			{
				return x + ", " + y;
			}
		}
	}
}