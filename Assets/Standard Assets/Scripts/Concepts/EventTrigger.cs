﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using System;
using Extensions;
using UnityEngine.Serialization;

namespace ControlShift
{
	//[ExecuteInEditMode]
	public class EventTrigger : MonoBehaviour//, IUpdatable
	{
		public UnityEvent onTriggerEnter2D;
		public UnityEvent onTriggerExit2D;
		// public UnityEvent onCollisionEnter2D;
		// public UnityEvent onCollisionExit2D;
		// public InputButtonTrigger[] inputButtonTriggers = new InputButtonTrigger[0];
		// public InputAxisTrigger[] inputAxisTriggers = new InputAxisTrigger[0];
		// public bool PauseWhileUnfocused
		// {
		// 	get
		// 	{
		// 		return true;
		// 	}
		// }
		// public bool runWhilePaused;

// 		public virtual void OnEnable ()
// 		{
// #if UNITY_EDITOR
// 			if (!Application.isPlaying)
// 				return;
// #endif
// 			if (inputButtonTriggers.Length > 0 || inputAxisTriggers.Length > 0)
// 				GameManager.updatables = GameManager.updatables.Add(this);
// 		}

// 		public virtual void OnDisable ()
// 		{
// #if UNITY_EDITOR
// 			if (!Application.isPlaying)
// 				return;
// #endif
// 			if (inputButtonTriggers.Length > 0 || inputAxisTriggers.Length > 0)
// 				GameManager.updatables = GameManager.updatables.Remove(this);
// 		}

// 		public virtual void DoUpdate ()
// 		{
// 			if (!runWhilePaused && GameManager.paused)
// 				return;
// 			foreach (InputButtonTrigger inputButtonTrigger in inputButtonTriggers)
// 				inputButtonTrigger.Update ();
// 			foreach (InputAxisTrigger inputAxisTrigger in inputAxisTriggers)
// 				inputAxisTrigger.Update ();
// 		}

		public virtual void OnTriggerEnter2D (Collider2D other)
		{
			onTriggerEnter2D.Invoke();
		}

		public virtual void OnTriggerExit2D (Collider2D other)
		{
			if (!gameObject.activeSelf)
				return;
			onTriggerExit2D.Invoke();
		}

		// public virtual void OnCollisionEnter2D (Collision2D coll)
		// {
		// 	onCollisionEnter2D.Invoke();
		// }

		// public virtual void OnCollisionExit2D (Collision2D coll)
		// {
		// 	if (!gameObject.activeSelf)
		// 		return;
		// 	onCollisionExit2D.Invoke();
		// }

		// [Serializable]
		// public class InputButtonTrigger
		// {
		// 	public InputButton button;
		// 	public InputState state; 
		// 	public UnityEvent unityEvent;

		// 	public virtual void Update ()
		// 	{
		// 		if (state == InputState.Down)
		// 		{
		// 			if (button.GetDown())
		// 				unityEvent.Invoke();
		// 		}
		// 		else if (state == InputState.Held)
		// 		{
		// 			if (button.Get())
		// 				unityEvent.Invoke();
		// 		}
		// 		else if (button.GetUp())
		// 			unityEvent.Invoke();
		// 	}
		// }

		// [Serializable]
		// public class InputAxisTrigger
		// {
		// 	public string axisName;
		// 	public InputState state;
		// 	public AxisRange axisRange; 
		// 	public UnityEvent unityEvent;

		// 	public virtual void Update ()
		// 	{
		// 		if (state == InputState.Down)
		// 		{
		// 			if (axisRange == AxisRange.Positive)
		// 			{
		// 				if (InputManager.inputter.GetAxis(axisName) > InputManager.Settings.defaultDeadzoneMin && InputManager.inputter.GetAxisPrev(axisName) <= InputManager.Settings.defaultDeadzoneMin)
		// 					unityEvent.Invoke();
		// 			}
		// 			else if (axisRange == AxisRange.Negative)
		// 			{
		// 				if (InputManager.inputter.GetAxis(axisName) < -InputManager.Settings.defaultDeadzoneMin && InputManager.inputter.GetAxisPrev(axisName) >= -InputManager.Settings.defaultDeadzoneMin)
		// 					unityEvent.Invoke();
		// 			}
		// 			else if (Mathf.Abs(InputManager.inputter.GetAxis(axisName)) > InputManager.Settings.defaultDeadzoneMin && Mathf.Abs(InputManager.inputter.GetAxisPrev(axisName)) <= InputManager.Settings.defaultDeadzoneMin)
		// 				unityEvent.Invoke();
		// 		}
		// 		else if (state == InputState.Held)
		// 		{
		// 			if (axisRange == AxisRange.Positive)
		// 			{
		// 				if (InputManager.inputter.GetAxis(axisName) > InputManager.Settings.defaultDeadzoneMin)
		// 					unityEvent.Invoke();
		// 			}
		// 			else if (axisRange == AxisRange.Negative)
		// 			{
		// 				if (InputManager.inputter.GetAxis(axisName) < -InputManager.Settings.defaultDeadzoneMin)
		// 					unityEvent.Invoke();
		// 			}
		// 			else if (Mathf.Abs(InputManager.inputter.GetAxis(axisName)) > InputManager.Settings.defaultDeadzoneMin)
		// 				unityEvent.Invoke();
		// 		}
		// 		else
		// 		{
		// 			if (axisRange == AxisRange.Positive)
		// 			{
		// 				if (InputManager.inputter.GetAxis(axisName) <= InputManager.Settings.defaultDeadzoneMin && InputManager.inputter.GetAxisPrev(axisName) > InputManager.Settings.defaultDeadzoneMin)
		// 					unityEvent.Invoke();
		// 			}
		// 			else if (axisRange == AxisRange.Negative)
		// 			{
		// 				if (InputManager.inputter.GetAxis(axisName) >= -InputManager.Settings.defaultDeadzoneMin && InputManager.inputter.GetAxisPrev(axisName) < -InputManager.Settings.defaultDeadzoneMin)
		// 					unityEvent.Invoke();
		// 			}
		// 			else if (Mathf.Abs(InputManager.inputter.GetAxis(axisName)) <= InputManager.Settings.defaultDeadzoneMin && Mathf.Abs(InputManager.inputter.GetAxisPrev(axisName)) > InputManager.Settings.defaultDeadzoneMin)
		// 				unityEvent.Invoke();
		// 		}
		// 	}

		// 	public enum AxisRange
		// 	{
		// 		Positive,
		// 		Negative,
		// 		Full
		// 	}
		// }

		public enum InputState
		{
			Down,
			Held,
			Up
		}
	}
}