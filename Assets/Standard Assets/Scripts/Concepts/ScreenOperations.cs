﻿using System;
using System.Runtime.InteropServices;
using System.Diagnostics;
using System.Drawing;
using Point = System.Drawing.Point;
using Color = System.Drawing.Color;

namespace ControlShift
{
	public class ScreenOperations
	{
		[DllImport("user32.dll")]
		static extern bool GetCursorPos(ref Point lpPoint);
		
		[DllImport("gdi32.dll", CharSet = CharSet.Auto, SetLastError = true, ExactSpelling = true)]
		public static extern int BitBlt(IntPtr hDC, int x, int y, int nWidth, int nHeight, IntPtr hSrcDC, int xSrc, int ySrc, int dwRop);
		
		public static Color GetColorAt (Point location)
		{
			Bitmap screenPixel = new Bitmap(1, 1);
			using (Graphics gdest = Graphics.FromImage(screenPixel))
			{
				using (Graphics gsrc = Graphics.FromHwnd(IntPtr.Zero))
				{
					IntPtr hSrcDC = gsrc.GetHdc();
					IntPtr hDC = gdest.GetHdc();
					int retval = BitBlt(hDC, 0, 0, 1, 1, hSrcDC, location.X, location.Y, (int) CopyPixelOperation.SourceCopy);
					gdest.ReleaseHdc();
					gsrc.ReleaseHdc();
				}
			}
			return screenPixel.GetPixel(0, 0);
		}
	}
}