﻿using UnityEngine;
using System.Collections;
using Extensions;

namespace ControlShift
{
	public class FreezeObject : MonoBehaviour
	{
		public Transform trs;
		public bool freezePosition;
		public bool freezeRelativePosition;
		public bool freezeRotation;
		public bool freezeScale;
		Vector3 initRota;
		Vector3 initPos;
		Vector3 initRelativePos;
		Vector3 initWorldScale;
		public static FreezeObject[] instances = new FreezeObject[0];

		public virtual void Awake ()
		{
			initRota = trs.eulerAngles;
			initPos = trs.position;
			initRelativePos = trs.position - trs.parent.position;
			initWorldScale = trs.lossyScale;
			instances = instances.Add(this);
		}

		public virtual void OnDestroy ()
		{
			instances = instances.Remove(this);
		}

		public virtual void DoUpdate ()
		{
			if (freezePosition)
				trs.position = initPos;
			else if (freezeRelativePosition)
				trs.position = trs.parent.position + initRelativePos;
			if (freezeRotation)
				trs.eulerAngles = initRota;
			if (freezeScale)
				trs.SetWorldScale (initWorldScale);
		}
	}
}