﻿#if UNITY_EDITOR
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace ControlShift
{
	[ExecuteInEditMode]
	public class DebugCursorPosition : MonoBehaviour
	{
		void OnEnable ()
		{
			EditorApplication.update += DoUpdate;
		}
		
		void OnDisable ()
		{
			EditorApplication.update -= DoUpdate;
		}
		
		void DoUpdate ()
		{
			Debug.Log(MouseOperations.GetCursorPosition());
		}
	}
}
#endif