﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using WindowsInput;
using WindowsInput.Native;
using System;
using System.Diagnostics;
using Extensions;

namespace ControlShift
{
	[Serializable]
	public class Macro : MonoBehaviour, IUpdatable
	{
		public Color color;
		public InputManager.InputButtonGroup executeButtons;
		public InputManager.InputButtonGroup recordButtons;
		public InputManager.InputButtonGroup playbackButtons;
		public List<Action> actions = new List<Action>();
		RecordState recordState = RecordState.NotRecording;
		PlaybackState playbackState = PlaybackState.NotPlaying;
		Action action;
		Action previousAction;
		int currentActionIndex;
		public MouseRecordMode mouseRecordMode;
		public KeysRecordMode keysRecordMode;
		bool recordMouseState;
		bool recordKeysState;

		public virtual void Start ()
		{
			GameManager.updatables = GameManager.updatables.Add(this);
		}

		public virtual void OnDestroy ()
		{
			GameManager.updatables = GameManager.updatables.Remove(this);
		}
	
		public virtual void SetValuesToMacro (Macro macro)
		{
			recordButtons = macro.recordButtons;
			playbackButtons = macro.playbackButtons;
			mouseRecordMode = macro.mouseRecordMode;
			keysRecordMode = macro.keysRecordMode;
		}
		
		public virtual void DoUpdate ()
		{
			HandleRecording ();
			HandlePlayback ();
			HandleExecute ();
		}
		
		public virtual void HandleRecording ()
		{
			if (recordButtons.AllButtonsActivated && !recordButtons.previousAllButtonsActivated)
			{
				if (recordState == RecordState.NotRecording)
					recordState = RecordState.Readying;
				else if (recordState == RecordState.Recording)
					recordState = RecordState.NotRecording;
			}
			else if (recordState == RecordState.Readying && !recordButtons.AnyButtonActivated)
				recordState = RecordState.Recording;
			if (recordState == RecordState.Recording)
				Record ();
			recordButtons.previousAllButtonsActivated = recordButtons.AllButtonsActivated;
		}
		
		public virtual void Record ()
		{
			action = new Action();
			action.mouseEventType = MouseOperations.MouseEventFlags.Absolute;
			action.point = MouseOperations.GetCursorPosition();
			if (!recordButtons.AnyButtonActivated)
			{
				for (int i = 0; i < GameManager.allKeys.Length; i ++)
				{
					VirtualKeyCode key = (VirtualKeyCode) GameManager.allKeys.GetValue(i);
					if (!GameManager.pressedKeys.Contains(key) && GameManager.inputDeviceStateAdaptor.IsKeyDown(key))
					{
						action.downKeys.Add(key);
						GameManager.pressedKeys.Add(key);
					}
					else if (GameManager.pressedKeys.Contains(key) && !GameManager.inputDeviceStateAdaptor.IsKeyDown(key))
					{
						action.upKeys.Add(key);
						GameManager.pressedKeys.Remove(key);
					}
				}
			}
			RecordActionIfShould (action);
		}
		
		public virtual void RecordActionIfShould (Action action)
		{
			if (keysRecordMode == KeysRecordMode.RecordAll)
				recordKeysState = true;
			else if (keysRecordMode == KeysRecordMode.OmitAll)
				recordKeysState = false;
			else if (keysRecordMode == KeysRecordMode.OmitKeyHoldTime)
				recordKeysState = action.upKeys.Count > 0 || action.downKeys.Count > 0;
			else if (keysRecordMode == KeysRecordMode.OmitKeyNotHeldTime)
				recordKeysState = GameManager.pressedKeys.Count > 0;
			else if (keysRecordMode == KeysRecordMode.RecordChangesOnly)
			{
				if (previousAction != null)
					recordKeysState = action.upKeys != previousAction.upKeys || action.downKeys != previousAction.downKeys;
				else
					recordKeysState = GameManager.pressedKeys.Count > 0;
			}
			if (mouseRecordMode == MouseRecordMode.RecordAll)
				recordMouseState = true;
			else if (mouseRecordMode == MouseRecordMode.OmitAll)
				recordMouseState = false;
			else if (mouseRecordMode == MouseRecordMode.RecordOnlyOnKeyPress)
				recordMouseState = action.downKeys.Count > 0;
			else if (mouseRecordMode == MouseRecordMode.RecordChangesOnly)
			{
				if (previousAction != null)
					recordKeysState = !action.point.Equals(previousAction.point);
				else
					recordKeysState = true;
			}
			if (recordMouseState || recordKeysState)
				RecordAction (action);
		}
		
		public virtual void RecordAction (Action action)
		{
			actions.Add(action);
			previousAction = action;
		}
		
		public virtual void HandlePlayback ()
		{
			if (playbackButtons.AllButtonsActivated && !playbackButtons.previousAllButtonsActivated)
			{
				if (playbackState == PlaybackState.NotPlaying)
					playbackState = PlaybackState.Readying;
				else if (playbackState == PlaybackState.Playing)
					playbackState = PlaybackState.NotPlaying;
			}
			else if (playbackState == PlaybackState.Readying && !playbackButtons.AnyButtonActivated)
				playbackState = PlaybackState.Playing;
			if (playbackState == PlaybackState.Playing)
				Playback ();
			playbackButtons.previousAllButtonsActivated = playbackButtons.AllButtonsActivated;
		}

		public virtual void HandleExecute ()
		{
			if (executeButtons.AllButtonsActivated)
			{
				executeButtons.isPressing = true;
				if (GameManager.pressedKeys.Count > executeButtons.buttons.Length)
					executeButtons.cancelled = true;
			}
			else if (executeButtons.isPressing && !executeButtons.AnyButtonActivated && executeButtons.previousAnyButtonsActivated)
			{
				if (!(executeButtons.cancellable && executeButtons.cancelled))
					Execute ();
				executeButtons.cancelled = false;
				executeButtons.isPressing = false;
			}
			executeButtons.previousAnyButtonsActivated = executeButtons.AnyButtonActivated;
		}
		
		public virtual void Execute ()
		{
			foreach (Action action in actions)
				action.Do ();
		}

		public virtual void Playback ()
		{
			if (currentActionIndex < actions.Count)
			{
				actions[currentActionIndex].Do ();
				currentActionIndex ++;
			}
			else
			{
				playbackState = PlaybackState.NotPlaying;
				currentActionIndex = 0;
			}
		}
		
		public virtual void OnApplicationQuit ()
		{
			foreach (VirtualKeyCode key in GameManager.pressedKeys)
				GameManager.inputSimulator.Keyboard.KeyUp(key);
		}
		
		public enum MouseRecordMode
		{
			RecordAll,
			RecordOnlyOnKeyPress,
			RecordChangesOnly,
			OmitAll
		}
	
		public enum KeysRecordMode
		{
			RecordAll,
			RecordChangesOnly,
			OmitKeyHoldTime,
			OmitKeyNotHeldTime,
			OmitAll
		}
		
		public enum RecordState
		{
			Readying,
			Recording,
			NotRecording
		}
		
		public enum PlaybackState
		{
			Readying,
			Playing,
			NotPlaying
		}
		
		[Serializable]
		public class Action
		{
			public string name;
			public MouseOperations.MouseEventFlags mouseEventType;
			public MouseOperations.MousePoint point;
			public List<VirtualKeyCode> downKeys = new List<VirtualKeyCode>();
			public List<VirtualKeyCode> upKeys = new List<VirtualKeyCode>();
			public string openFileWithName;
			public string openFileArgs;

			public virtual void Do ()
			{
				if (mouseEventType == MouseOperations.MouseEventFlags.Move || mouseEventType == MouseOperations.MouseEventFlags.Absolute)
					MouseOperations.SetCursorPosition(point);
				else
					MouseOperations.MouseEvent (mouseEventType);
				foreach (VirtualKeyCode key in downKeys)
					GameManager.inputSimulator.Keyboard.KeyDown(key);
				foreach (VirtualKeyCode key in upKeys)
					GameManager.inputSimulator.Keyboard.KeyUp(key);
				if (!string.IsNullOrEmpty(openFileWithName))
					Process.Start(openFileWithName, openFileArgs);
			}
		}
	}
}